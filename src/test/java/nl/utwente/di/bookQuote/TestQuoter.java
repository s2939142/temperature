package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class TestQuoter {
    @Test
    public void TestBook1() throws Exception{
        Calculator calculator = new Calculator();
        double price = calculator.getFahrenheit( 0);
        Assertions.assertEquals( 32 , price , 0.0 , "Temperature in Fahrenheit" ) ;
    }
}